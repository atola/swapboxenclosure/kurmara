![atola](images/logo.png)


https://atola.net

## Kurmara Enclosure

##### Kurmara Enclosure is the perfect enclosure fit with the swap-box project, it is made for cash in and cash out.


Freecad Kurmara
=======

Atola has chosen to work only with open source tools. Freecad is just perfect for 3d plans plus 2d plans for machine cuts.

https://www.freecadweb.org/


![Freecad Kurmara](images/kurmara.png)

The Kurmara model has not yet been built, it is a prototype for the moment, all your observations are welcome


[PDF Plans](Plans_pdf/full_plans(1to10).pdf)

### Help us

We really need your help either in the form of coding, or in planning, testing, testing, or in the form of a donation, so far we have managed to not have an outside investor for speculative purposes. , this project is completely open to everyone and we want to stay that way.


https://atola.net/donate


Any question ? your are welcome / info@atola.ch
